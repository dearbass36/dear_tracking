require("@nomiclabs/hardhat-waffle");

const ROPSTEN_URL = "https://rinkeby.infura.io/v3/3e8eff60b3bb41cb95bef1c8e16c4a33"
const PRIVATE_KEY = "caf3fbf1e22a138eccfc74f328bde91e7234d012c032658d4cac6aeeaed77189"

// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async () => {
  const accounts = await ethers.getSigners();

  for (const account of accounts) {
    console.log(account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.0",
  artifacts: "./src/artifacts",
  networks: {
    hardhat: {
      chainId: 1337
    },
    rinkeby: {
      url: ROPSTEN_URL, //infura url
      accounts: [`0x${PRIVATE_KEY}`] //privateKey + 0x
    }
  }
};

