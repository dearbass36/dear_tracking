const { expect } = require("chai");
import "hardhat/console.sol";
const { console } = require("hardhat");

describe("Item", function() {
    let ItemFactory, itemFactory, ItemOwner ,itemOwner, owner, addr1;
    beforeEach(async () => {
      ItemFactory = await ethers.getContractFactory("ItemFactory");
      itemFactory = await ItemFactory.deploy();
      await itemFactory.deployed();

      ItemOwner = await ethers.getContractFactory("ItemOwnerShip");
      itemOwner = await ItemOwner.deploy();
      await itemOwner.deployed();
      [owner, addr1] = await ethers.getSigners();
    });

    describe("Item Deployment", () => {
      it("Item contract has a name", async () => {
        expect(await itemFactory.name()).to.equal("DearTracking Instant");
      });
    });
  
});
