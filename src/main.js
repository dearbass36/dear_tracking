import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import mixin from "./mixins";
import moment from "moment-timezone";
import 'material-design-icons-iconfont';
import { ethers, Signer } from "ethers";
import ItemFactory from "./artifacts/contracts/ItemFactory.sol/ItemFactory.json";
import WarehouseFactory from "./artifacts/contracts/WarehouseFactory.sol/WarehouseFactory.json"

Vue.prototype.$ethers = ethers;
Vue.prototype.$factory_abi = ItemFactory.abi;
Vue.prototype.$factory_address = "0x60e711d8CCDA26f4FDCdd6d3b2dff7Bc92cBBE69";
Vue.prototype.$warehouse_abi = WarehouseFactory.abi;
Vue.prototype.$warehouse_address = "0x5cb11aA03F1B7B516769fE8CAF37B637E5A86f81";

moment()
  .tz("Asia/Bangkok")
  .format();
Vue.prototype.$moment = moment;
Vue.config.productionTip = false
Vue.mixin(mixin);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
