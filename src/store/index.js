import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    contracts: null,
    account: null,
    balance: "0",
    balance_eth: "0",
    loadingStatePage: true
  },
  getters: {
    contracts: (state) => state.contracts,
    account: (state) => state.account,
    balance: (state) => state.balance,
    balance_eth: (state) => state.balance_eth,
    loadingStatePage: (state) => state.loadingStatePage
  },
  mutations: {
    SET_LOADING(state, data) {
      state.loadingStatePage = data;
    },
    SET_CONTRACTS(state, data) {
      state.contracts = data;
    },
    SET_ACCOUNT(state, data) {
      state.account = data;
    },
    SET_BALANCE(state, data) {
      state.balance = data;
    },
    SET_BALANCE_ETH(state, data) {
      state.balance_eth = data;
    }
  },
  actions: {},
  modules: {},
});
