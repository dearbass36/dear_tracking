import store from "../store";

const mixins = {
  computed: {
    account() {
      return store.getters.account
    },
    contracts() {
      return store.getters.contracts
    },
    balance() {
      return store.getters.balance;
    },
    balance_eth() {
      return store.getters.balance_eth
    },
    loadingStatePage() {
      return store.getters.loadingStatePage;
    }
  },
  data: function () {
    return {
      message: 'goodbye',
      bar: 'def'
    }
  },
  methods: {
    setLoadingPage(state) {
      store.commit("SET_LOADING", state);
    }
  }
};

export default mixins;