const hre = require("hardhat");

async function main() {
  const ItemFactory = await hre.ethers.getContractFactory("ItemFactory");
  const itemFactory = await ItemFactory.deploy();
  await itemFactory.deployed();
  console.log("ItemFactory deployed to:", itemFactory.address);
  const WarehouseFactory = await hre.ethers.getContractFactory("WarehouseFactory");
  const warehouseFactory = await WarehouseFactory.deploy();
  await warehouseFactory.deployed();
  console.log("WarehouseFactory deployed to:", warehouseFactory.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
