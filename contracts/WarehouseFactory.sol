//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract WarehouseFactory {
    event NewWarehouse(string name, uint id);

    struct Warehouse {
        uint warehouseId;
        string name;
    }

    struct Tracking {
        uint warehouseId;
        CheckIn checkIn;
        CheckOut checkOut;
    }

    struct CheckIn {
        string receiverName;
        uint date;
        bool status;
    }

    struct CheckOut {
        string receiverName;
        uint date;
        bool status;
    }

    mapping(uint => address) public warehouseToOwner;
    mapping(address => uint) ownerWarehouseCount;
    Warehouse[] public warehouses;

    function createWarehouse(string memory _name) public {
        warehouses.push();
        uint id = warehouses.length - 1;
        warehouses[id].warehouseId = id;
        warehouses[id].name = _name;
        warehouseToOwner[id] = msg.sender;
        ownerWarehouseCount[msg.sender]++;
        emit NewWarehouse(_name, id);
    }

    function getWarehouseByOwner(address _owner) public view
        returns (Warehouse[] memory)
    {
        Warehouse[] memory result =
            new Warehouse[](ownerWarehouseCount[_owner]);
        uint counter = 0;
        for (uint i = 0; i < warehouses.length; i++) {
            if (warehouseToOwner[i] == _owner) {
                result[counter] = warehouses[i];
                counter++;
            }
        }
        return result;
    }
}
