//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "./WarehouseFactory.sol";

// import "@openzeppelin/contracts/access/Ownable.sol";
// import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract ItemFactory is WarehouseFactory{
    event NewItem(uint orderNumber, bool status);
    event TrackedCheckIn(string _reciev, uint indexed _itemId, uint _warehouseId);
    event TrackedCheckOut(string _reciev, uint indexed _itemId, uint _warehouseId);

    string public name = "DearTracking Instant";

    uint randNonce = 0;

    function randOrderNumber(uint _modulus) internal returns (uint) {
        // increase nonce
        randNonce++;
        return
            uint(
                keccak256(
                    abi.encodePacked(block.timestamp, msg.sender, randNonce)
                )
            ) % _modulus;
    }

    struct Item {
        uint itemId;
        uint orderNumber;
        string itemName;
        string senderName;
        bool status;
        Tracking[] tracking;
    }

    mapping(uint => address) public itemToOwner;
    mapping(uint => uint) public itemByIdOrderNumber;
    mapping(address => uint) ownerItemCount;

    Item[] public items;

    function newItem(string memory _senderName,string memory _itemName) public {
        items.push();
        uint index = items.length - 1;
        uint orderNumber = randOrderNumber(10000000);
        items[index].itemId = index;
        items[index].orderNumber = orderNumber;
        items[index].itemName = _itemName;
        items[index].senderName = _senderName;
        items[index].status = false;
        itemToOwner[index] = msg.sender;
        ownerItemCount[msg.sender]++;
        itemByIdOrderNumber[orderNumber] = index;
        emit NewItem(orderNumber, false);
    }

    function updateStatusItem(uint _index) public {
        Item storage updateItem = items[_index];
        updateItem.status = true;
    }

    function getItemsByOwner(address _owner)
        external
        view
        returns (Item[] memory)
    {
        Item[] memory result = new Item[](ownerItemCount[_owner]);
        uint counter = 0;
        for (uint i = 0; i < items.length; i++) {
            if (itemToOwner[i] == _owner) {
                result[counter] = items[i];
                counter++;
            }
        }
        return result;
    }

    function ItemCheckIn(uint _warehouseId, uint _orderNumber, string memory _reciev) external {
      uint itemId = itemByIdOrderNumber[_orderNumber];
      Item storage itemTracking = items[itemId];
      itemTracking.tracking.push();
      uint index = itemTracking.tracking.length - 1;
      itemTracking.tracking[index].warehouseId = _warehouseId;
      itemTracking.tracking[index].checkIn = CheckIn(_reciev, block.timestamp,true);
      emit TrackedCheckIn(_reciev,itemId,_warehouseId);
    }

    function ItemCheckOut(uint _warehouseId, uint _orderNumber, string memory _reciev) external {
      uint itemId = itemByIdOrderNumber[_orderNumber];
      Item storage itemTracking = items[itemId];
      uint index = itemTracking.tracking.length - 1;
      require(itemTracking.tracking[index].checkIn.status);
      itemTracking.tracking[index].warehouseId = _warehouseId;
      itemTracking.tracking[index].checkOut = CheckOut(_reciev, block.timestamp,true);
      emit TrackedCheckOut(_reciev,itemId,_warehouseId);
    }

    function CustomerGetByOrderNumber(uint orderNumber) external view returns(Item memory) {
        uint index = itemByIdOrderNumber[orderNumber];
        return items[index];
    }
}
